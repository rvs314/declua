local M = {}

local u = require "util"
local i = require "indents"
local e = require "escape"

-- Write some data to DEC
function M.to(data)
    local out = ""

    -- For each of the key-value pairs
    for key, value in pairs(data) do
        -- Append the key
        out = out .. e.dirty(tostring(key)) .. ":"
        if type(value) ~= "table" then
            -- If it's not a table, just print the tostring'ed value
            out = out .. " " .. e.dirty(tostring(value)) .. "\n"
        elseif u.isArr(value) then
            -- If it's an array, just print out each value in the array with an indent
            out = out .. "\n"
            for _, s in ipairs(value) do
                out = out .. i.STD_INDENT .. e.dirty(tostring(s)) .. "\n"
            end
        else
            -- Else (it's a KVP-list), translate the value to a string and append it with indents on each line
            out = out .. "\n"
            rec = M.to(value)
            for l in rec:gmatch("([^\n]+)") do
                out = out .. i.STD_INDENT .. l .. "\n"
            end
        end
    end

    return out:sub(1, #out-1)
end

return M
