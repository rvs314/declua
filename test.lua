local dec = require "dec"

-- Requires pprint, which you can get here: github.com/jagt/pprint.lua
local pp = require "pprint"

a = {
    one   = "two",
    three = { 3, 3.5, 3.75 },
    five  = { six = "7" ,
              eight = 9},
}

b = [[
# What am I doing
message:
    from: Bugs Bunny
    to: Doc
    text: What's up? λ
]]

function stringAndBack(str)
    print("Starting DEC string:\n")
    print(b)
    print("Is parsed to:\n")
    conv = dec.from(b)
    print(pp.pprint(conv))
    print("Is parsed back to:\n")
    print(dec.to(conv))
end

function tableAndBack(tab)
    print("Starting table:\n")
    print(pp.pprint(tab))
    print("Is parsed to:\n")
    conv = dec.to(tab)
    print(conv)
    print("Is parsed back to:\n")
    print(pp.pprint(dec.from(conv)))
end

tableAndBack(a)
