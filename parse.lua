local M = {}

local esc = require "escape"
local util = require "util"

-- Does a given line have anything but whitespace?
function M.isEmpty(str)
    return str:gsub(" ",""):gsub("\t","") == ""
end

-- Pattern for KVPs
-- Note: Capture groups include beginning/ending whitespace, which should be trimmed
local KVPPat = "^(.*[^"..esc.CHAR_REP.."]):(.*)$"

-- Is a line a KVP?
function M.isKVP(str) 
    return str:match(KVPPat) and true
end

-- Given a KVP, return the key and value
function M.parseKVP(str)
    k, v = str:match(KVPPat)
    return util.trim(k), util.trim(v)
end

return M
