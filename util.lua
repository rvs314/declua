local M = {}

-- Return a table with a string split by a seperator
function M.split(inputstr, sep)
    sep = sep or "%s"
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

-- Remove whitespace (%s) from beginning and end of a string
function M.trim(s)
   return s:match("^%s*(.-)%s*$")
end

--[[
    Checks if a table is a valid array
    Note: dec-lua doesn not allow nil values in an array, or else it's thought of as a table.
          It ensure that each of the elements from one to the length of the array is non-nil.
]]--
function M.isArr(data)
    local len = M.tLen(data)
    for i = 1, len do
        if data[i] == nil then 
            return false
        end
    end
    return true
end

-- Given a table, find its length
function M.tLen(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

-- Is a string empty?
function M.isEmpty(s)
    return s:match("^%s*$") and true
end

return M
