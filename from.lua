local u = require "util"
local e = require "escape"
local p = require "parse"
local i = require "indents"

local M = {}

local index = 1
local lines = {}
local len = 0

-- This uses a lot of globals - beware

-- Get DEC from text
function M.from(text)
    lines = u.split(text, "\n")
    len = u.tLen(lines)
    index = 1
    return parseKVP()
end

-- What is the KVP at the current $index of $lines
-- Changes index and requires lines
function parseKVP()

    if skipBlanks() then
        return {}
    end

    local values = {}
    local startLvl = i.getIndent(e.clean(lines[index]))

    while index <= len do
        local indLvl, line = i.parseLine(lines[index])
        line = e.clean(line)

        if indLvl > startLvl then
            error(string.format("Indent error on line %d: \"%s\"", index, e.actual(line)))
        end

        if indLvl < startLvl then
            index = index - 1
            return values
        end

        if not p.isKVP(line) then
           error(string.format("Found invalid non-KVP data on line %d: \"%s\"", index, e.actual(line)))
        end

        local key, rawval = p.parseKVP(line)
        local value

        if u.isEmpty(rawval) then
            index = index + 1

            if index > u.tLen(lines) then
                error(string.format("Indent required after open colon on line %d: \"%s\"", index-1, e.actual(line)))
            end

            if p.isKVP(lines[index]) then
                value = parseKVP()
            elseif i.getIndent(lines[index]) > startLvl then
                value = parseArr()
            else
                error(string.format("Open colon without indent on line %d: \"%s\"", index-1, e.actual(line)))
            end
        else
            value = e.actual(rawval)
        end

        values[e.actual(key)] = value

        index = index + 1
    end

    return values
end

-- What is the value of the array at $index in $lines
function parseArr()
    local values = {}

    if skipBlanks() then
        return values
    end

    local startLvl = i.getIndent(lines[index])

    while index <= len do

        local indLvl, line = i.parseLine(lines[index])
        line = e.clean(line)

        if indLvl > startLvl then
            error(string.format("Indent error on line %d: \"%s\"", index, e.actual(line)))
        end

        if indLvl < startLvl then
            index = index - 1
            return values
        end

        values[#values+1] = e.actual(line)

        if skipBlanks() then
            return values
        end

        index = index + 1
    end

    return values
end

-- Moves $index forward until it doesn't point to a blank line
-- Returns true if it hit the bottom of index, false otherwise
function skipBlanks()
    while index <= len do
        if p.isEmpty(e.clean(lines[index])) then
            index = index + 1
        else 
            return false 
        end
    end
    return true
end

return M
