local M = {}

M.TAB_SIZE = 4 -- The number of spaces a tab should be
M.STD_INDENT = "    " -- A standard indent (4 spaces)

-- Given a line, return how many spaces it is indented with and its contents without starting indent
function M.parseLine(data)
    local ind = 0
    local i = 1
    for c in data:gmatch(".") do
        if c == " " then 
            ind = ind + 1 
        elseif c == "\t" then 
            ind = int + M.TAB_SIZE 
        else 
            return ind, data:sub(i)
        end
        i = i + 1
    end
    return ind, data:sub(i)
end

-- Given a line, return how many space it's indented with
function M.getIndent(data)
    local ind = 0
    for c in data:gmatch(".") do
        if c == " " then 
            ind = ind + 1 
        elseif c == "\t" then 
            ind = int + M.TAB_SIZE 
        else 
            return ind
        end
    end
    return ind
end

return M
