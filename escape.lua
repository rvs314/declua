local M = {}

-- Constant to replace the ':' and '#' characters when escaped
M.CHAR_REP="\u{FFFD}"

-- Takes a doubly-escaped strings and makes it absolutely escaped
function M.escape(str)
    return str:gsub("::", M.CHAR_REP .. ":"):gsub("##", M.CHAR_REP .. "#")
end

-- Remove coments for an absolutely escaped string
function M.censor(str)
    return str:gsub("[^"..M.CHAR_REP.."]#.*$",""):gsub("^#.*$","")
end

-- Absolutely escape then censor a string
function M.clean(str)
    return M.censor(M.escape(str))
end

-- Return an absolutely escaped string's value
function M.actual(str)
    return str:gsub(M.CHAR_REP,"")
end

-- Given a totally unescaped string, return a doubly-escaped version
function M.dirty(str)
    return str:gsub(":","::"):gsub("#","##")
end

return M
